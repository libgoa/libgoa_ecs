package libgoa.ecs;

class Entity extends h2d.Object implements libgoa.IBuilderSuperClass {

	var components:Map<String, Component>;

	public var id(default, set):ItzulId = '';
	public var initPos(default, set):h3d.Vector;
	public var width(get, null):Float;
	public var height(get, null):Float;
	public var entityParent(default, set):h2d.Object;
	public var active(default, null):Bool = false;

	var loaded:Bool = false;
	var inited:Bool = false;
	
	static public var all:Array<Entity> = [];

	public function new() {
		super();

		components = new Map();
		all.push(this);
	}

	public function addComponent(c:Component) {
		c.entity = this;
		components.set(c.id, c);
	}

	public function getComponent(cid:String) {
		return components.get(cid);
	}

	public function hasComponent(cid:String) {
		return getComponent(cid) != null;
	}

	public function removeComponent(cid:String) {
		var c = getComponent(cid);
		if (c == null)
			return;

		c.entity = null;
		components.remove(cid);
	}

	public function load() {
		if (loaded)
			return;
		updateLocalization();
		loaded = true;
	}

	public function start() {
		updateLocalization();

		active = visible = true;
		if (entityParent != null)
			entityParent.addChild(this);

		if (components != null)
			for (c in components)
				c.start();
	}

	public function unload() {
		if (!loaded)
			return;
		loaded = false;
	}

	public function stop() {
		active = visible = false;
		remove();

		if (components != null)
			for (c in components)
				c.stop();
	}

	public function updateLocalization() {
		if (!inited)
			return;
	}

	public function update(dt:Float) {
		if (!active)
			return;

		for (c in components)
			c.update(dt);
	}

	public function set_id(_id:ItzulId) {
		id = _id;
		updateLocalization();
		return _id;
	}

	public function set_initPos(p:h3d.Vector) {
		if (p != null)
			setPosition(p.x, p.y);
		
		return initPos = p;
	}

	public function get_width()
		return getSize().width;

	public function get_height()
		return getSize().height;

	public function set_entityParent(p:h2d.Object) {
		if (p != null && active)
			p.addChild(this);
		
		return entityParent = p;
	}
}
