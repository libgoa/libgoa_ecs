package libgoa.ecs.utils;

import haxe.macro.Context;
import haxe.macro.Expr;

class Macros {
	static public function buildComponent() {
		var fields = Context.getBuildFields();
		var localClass = Context.getLocalClass().get();
		var className = localClass.name;
		var idExpr = {
			name: 'ID',
			kind: FVar(macro:String, macro $v{className}),
			access: [AStatic, APublic, AInline],
			pos: Context.currentPos()
		};
		fields.push(idExpr);

		for (f in fields) {
			if (f.name == 'new') {
				switch f.kind {
					case FFun(f):
						switch f.expr.expr {
							case EBlock(b): b.push(macro id = $i{idExpr.name});
							case _:
						}
					case _:
				}
			}
		}

		return fields;
	}
}
