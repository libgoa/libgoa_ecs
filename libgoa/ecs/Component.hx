package libgoa.ecs;

@:autoBuild(libgoa.ecs.utils.Macros.buildComponent())
class Component implements libgoa.IBuilder {
	public var entity(default, set):Entity;
	public var id:String;
	public var active(default, set):Bool = false;

	public function new() {}

	public function start() {
		if (entity == null)
			throw 'entity must be non null';
		
		active = true;
	}

	public function reset() {
		stop();
		start();
	}

	public function stop() {
		active = false;
	}

	public function update(dt:Float) {}

	public inline function set_entity(e:Entity) {
		return entity = e;
	}

	public inline function set_active(a:Bool) {
		return active = a;
	}
}
